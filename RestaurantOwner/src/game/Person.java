/**
 * @(#) Person.java
 */

package game;

import lombok.Data;

@Data
public abstract class Person
{
	protected String name;
	
	protected String surname;
	
	
}
