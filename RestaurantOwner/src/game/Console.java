package game;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Console {
	
	public static String readLine( ) throws Exception
	{
		BufferedReader bufferRead = new BufferedReader(
				new InputStreamReader(System.in));
		return bufferRead.readLine();
	}

}
