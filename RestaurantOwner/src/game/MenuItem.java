/**
 * @(#) MenuItem.java
 */

package game;

import lombok.Data;

@Data
public abstract class MenuItem {
	protected String name;
	protected int price;
	private ItemQuality quality = ItemQuality.LOW;
	protected int cost;

}
