/**
 * @(#) Employee.java
 */

package game;

import lombok.Data;

@Data
public abstract class Employee extends Person {
	protected int salary = 300;
	protected static int TrainCost = 1200;
	protected ExpLevel expLevel = ExpLevel.LOW;

	public boolean trainEmployee( int budget ) {
		if (this.getExpLevel() != ExpLevel.HIGH && budget - TrainCost >= 0) {
			switch (this.getExpLevel()) {
			case LOW:
				this.setExpLevel(ExpLevel.MEDIUM);
				this.salary = 400;
				return true;
			case MEDIUM:
				this.setExpLevel(ExpLevel.HIGH);
				this.salary = 500;
				return true;
			default:
				break;
			}
		}
		return false;
	}
}
