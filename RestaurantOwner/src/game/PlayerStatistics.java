/**
 * @(#) PlayerStatistics.java
 */

package game;

import java.util.ArrayList;
import java.io.PrintWriter;
import java.util.Comparator;

import lombok.Data;

import java.io.File;
import java.util.Collections;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;

@Data
public class PlayerStatistics {
	private int numberOfPlayers;
	private String playerName;
	private int playerRanks;
	private int budgetAmount;


	String filePath = "./statistics.txt";

	File statFile = new File(filePath);

	
	private RankingList rankList;
	
	public PlayerStatistics( String name, int budget ) {
		this.playerName = name;
		this.budgetAmount = budget;
	}

	public void createStatistics( ) throws IOException {
		ArrayList<RankingList> playersList = new ArrayList<RankingList>();
		try {
			if (statFile.exists() == false) {
				System.out.println("We had to make a new file.");
				statFile.createNewFile();
			}
			PrintWriter out = new PrintWriter(new FileWriter(statFile, true));
			out.append(playerName + "," + budgetAmount + ","
					+ System.getProperty("line.separator"));
			out.close();
			playersList = getRankingList();
			Collections.sort(playersList, new RankComparator());
			int i = 0;
			for (RankingList player : playersList) {
				i++;
				player.setRank(i);
				System.out.println(player.getRank() + " - " + player.getPlayerName() + " - " + player.getBudget());
			}
		} catch (IOException e) {
			System.out.println("Error Creating Statistics");
		}
	}

	public ArrayList getRankingList( ) throws IOException {
		Scanner read = new Scanner(statFile);
		ArrayList<RankingList> rankingList = new ArrayList<RankingList>();
		read.useDelimiter("\\s*,\\s*");
		while (read.hasNext()) {
			RankingList rankLst = new RankingList();
			rankLst.setPlayerName(read.next());
			rankLst.setBudget(Integer.parseInt(read.next()));
			rankingList.add(rankLst);
		}
		read.close();
		return rankingList;
	}
	
	public class RankComparator implements Comparator<RankingList> {

		@Override
		public int compare(RankingList o1, RankingList o2) {
			return o2.getBudget().compareTo(o1.getBudget());
		}
	}

}
