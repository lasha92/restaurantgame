/**
 * @(#) Table.java
 */

package game;

import lombok.Data;

@Data
public class Table {
	private int number;
	private int income = 0;
	private boolean isAssigned = false;
	private boolean isOccupied = false;


	public Table( int number ) {
		this.number = number;

	}

}
