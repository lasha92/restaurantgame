/**
 * @(#) Orders.java
 */

package game;

import java.util.Random;
import lombok.Data;
import java.text.MessageFormat;

@Data
public class Orders {

	private Dish dish;
	private Baverage baverage;

	public Orders( Dish dish, Baverage baverage ) {
		this.dish = dish;
		this.baverage = baverage;
	}

	public String calculateFeedback( ) {
		return MessageFormat.format(
				"Baverage: name {0}, volume {1}\nDish: name {2}, calories {3}",
				baverage.getName(),baverage.getVolume(),dish.getName(),dish.getCalories());
	
	}

	public int calculateIncome( ) {
		return dish.getPrice() + baverage.getPrice();

	}

}
