/**
 * @(#) W
aiter.java

 */

package game;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Waiter extends Employee {

	private static int TrainWaiter = 800;
	private List<Table> tables;

	public Waiter( String name ) {
		setName(name);
		this.setSalary(200);
		tables = new ArrayList<Table>();
	}

	public void assignTable( ) {

	}

	public void serveClient( ) {

	}

	@Override
	public boolean trainEmployee( int budget ) {

		if (this.getExpLevel() != ExpLevel.HIGH && budget - TrainWaiter >= 0) {
			switch (this.getExpLevel()) {
			case LOW:
				this.setExpLevel(ExpLevel.MEDIUM);
				this.setSalary(300);
				return true;
			case MEDIUM:
				this.setExpLevel(ExpLevel.HIGH);
				this.setSalary(400);
				return true;
			default:
				break;
			}
		}

		return false;
	}

}
