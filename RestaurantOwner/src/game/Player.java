/**
 * @(#) Player.java
 */

package game;

import java.io.BufferedReader;
import lombok.Data;
import java.text.MessageFormat;
import java.io.InputStreamReader;
import java.io.IOException;

@Data
public class Player {
	private String name;

	private int moneyEarned;

	private int rank;

	private Restourant restaurant;

	private PlayerStatistics statistics;


	public void enterName( ) throws Exception {
		System.out.println("Enter your name : ");

		try {

			name = Console.readLine();

			System.out.println(MessageFormat.format(
					"Player {0} is Restourant owner", name));
			restaurant = new Restourant(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
