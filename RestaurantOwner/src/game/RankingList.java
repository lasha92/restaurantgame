package game;

import lombok.Data;

@Data
public class RankingList {
	
	private String playerName;
	private Integer budget;
	private Integer Rank;

}
