/**
 * @(#) Restourant.java
 */

package game;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import lombok.Data;

@Data
public class Restourant {
	private java.util.List<Client> clients;
	private int weekDayes = 0;
	private int BudgetAmount = 10000;
	public int reputation = 15;
	public int income;
	public int Name;
	public int Address;
	int days;
	public int City;
	private Menu menu;
	private List<Waiter> waiters;
	private Barman barman;
	private String msg;
	private Chef chef;
	private List<Table> tables;
	private int sumTableInc = 0;
	private int ingredientCost = 0;
	private String playerName;
	String[] employees = {"Jill","Tom","Brandon"};

	public Restourant( String n ) throws NumberFormatException, Exception {
		barman = new Barman("Bob");
		chef = new Chef("Jim", 123);
		waiters = new ArrayList<Waiter>();
		tables = new ArrayList<Table>();
		clients = new ArrayList<Client>();
		menu = new Menu();
		menu.createMenu(chef, barman);
		this.playerName = n;
		startGame();
	}

	public void isSufficientBudget( ) {
		if (this.BudgetAmount <= 0)
			gameOver();
	}

	public void chooseClients( int tableNum ) {

		Random rnd = new Random();
		Client client;
		if (tables.get(tableNum).isAssigned()) {
			for (int i = 0; i < 2; i++) {
				client = clients.get(rnd.nextInt(clients.size()-1));
				client.makeOrder(
						menu.getBaverages().get(
								rnd.nextInt(menu.getBaverages().size() - 1)),
						menu.getDishes().get(
								rnd.nextInt(menu.getDishes().size() - 1)));
				client.setBill(client.getOrder().calculateIncome());
				this.sumTableInc += client.getBill();
				this.reputation += client.calculateSatisfactory(waiters.get(rnd
						.nextInt(waiters.size() - 1)));

			}
			tables.get(tableNum).setIncome(this.sumTableInc);
			tables.get(tableNum).setOccupied(true);

		}

	}

	private void startGame( ) throws NumberFormatException, Exception {

		for (int i = 0; i < 18; i++) {
			clients.add(new Client("Test"));
		}

		for (int i = 0; i < 3; i++) {

			waiters.add(new Waiter(employees[i]));
		}

		for (int i = 0; i <= 9; i++) {
			tables.add(new Table(i));
		}

		assignTable();
		endOfTheDay();

	}

	private void assignTable( ) throws Exception {
		int tableNumber = 0;
		for (Waiter waiter : waiters) {
			System.out.println(MessageFormat.format("Selected waiter is {0}",
					waiter.getName()));

			for (int i = 0; i < 3; i++) {
				System.out.println("Enter Table Number:");
				String str = Console.readLine();
				if (invalidNumericCharacter(str)) {
					tableNumber = Integer.parseInt(str);
				}
				if (tables.get(tableNumber).isAssigned() == false) {
					waiter.getTables().add(tables.get(tableNumber));
					tables.get(tableNumber).setAssigned(true);
					System.out.println("Table assigned");
				} else {
					System.out.println("Incorrect Number");
					i--;
				}
			}
		}
	}

	public void trainEmployee( ) throws Exception {
		System.out.println("Do you want to train employee? [y/n]");
		String decision = Console.readLine().toLowerCase();
		while (decision.equals("y")) {
			System.out.println();
			System.out
					.println("Whom do you want to train (or Q to exit)?[W|B|C|Q]");
			switch (Console.readLine().toUpperCase()) {
			case "W":
				for (Waiter waiter : waiters) {
					System.out.println(MessageFormat.format(
							"Would you like to train waiter {0} [Y|N] ",
							waiter.getName()));
					if (Console.readLine().toUpperCase().equals("Y")) {
						if (waiter.getExpLevel() != ExpLevel.HIGH) {
							String msg = waiter.trainEmployee(BudgetAmount) ? MessageFormat
									.format("Waiter experience level upgraded to {0}",
											waiter.getExpLevel())
									: "You don't have enough money to train waiter";
							System.out.println(msg);
						} else {
							System.out
									.println(MessageFormat
											.format("Maximal level of experience for {0} is reached",
													waiter.getName()));
							break;
						}
					}
				}
				break;
			case "B":
				if (barman.getExpLevel() != ExpLevel.HIGH) {
					msg = barman.trainEmployee(BudgetAmount) ? MessageFormat
							.format("Barman experience level upgraded to {0}",
									barman.getExpLevel())
							: "You don't have enough money to train barman";
					System.out.println(msg);
				} else {
					System.out.println(MessageFormat.format(
							"Maximal level of experience for {0} is reached",
							barman.getName()));
					break;
				}
				break;

			case "C":
				if (barman.getExpLevel() != ExpLevel.HIGH) {
					msg = chef.trainEmployee(BudgetAmount) ? MessageFormat
							.format("Chef experience level upgraded to {0}",
									chef.getExpLevel())
							: "You don't have enough money to train chef";
					System.out.println(msg);
				} else {
					System.out.println(MessageFormat.format(
							"Maximal level of experience for {0} is reached",
							barman.getName()));
					break;
				}
				break;
			case "Q":
				decision = "n";
			}
		}
	}

	public void paySalary( ) {
		int salary = 0;
		salary += (barman.getSalary() + chef.getSalary());
		for (Waiter waiter : waiters) {
			salary += waiter.getSalary();
		}
		this.BudgetAmount -= salary;
		isSufficientBudget();

	}

	public void paySuppliers( ) {

		for (int i = 0; i < 5; i++) {
			ingredientCost += menu.getBaverages().get(i).getCost();
			ingredientCost += menu.getDishes().get(i).getCost();
		}

		this.BudgetAmount -= ingredientCost;
		isSufficientBudget();

	}

	public void gameOver( ) {
		System.out.println("Game Over!");
	}

	public void payAdditionalCosts( ) {

		this.BudgetAmount -= 4000;
		isSufficientBudget();
	}

	public void endOfTheDay( ) throws Exception {
		
		for(days = 1; days <= 30; days++)
		{
			if(days%7 == 0)
			{
				paySalary();
				trainEmployee();
				paySuppliers();
				System.out.println(MessageFormat.format(
						"End of the week budget {0}, reputation {1} ",
						this.BudgetAmount, this.reputation));
				
				for (Table table : tables)
					table.setAssigned(false);
				assignTable();
			}
			if(days == 30)
			{
				payAdditionalCosts();
			}
			for (Table table : tables) {
				if (table.isOccupied())
					this.BudgetAmount += table.getIncome();
			}
			occupyTables();
			
		}
		PlayerStatistics stats = new PlayerStatistics(playerName, BudgetAmount);
		stats.createStatistics();
		provideStatistics();

	}

	private void occupyTables( ) throws Exception {

		if (this.reputation >= 30)
			for (int i = 0; i < 9; i++)
				chooseClients(i);
		else if (this.reputation >= 15)
			for (int i = 0; i < 5; i++)
				chooseClients(i);
		else
			for (int i = 0; i < 2; i++)
				chooseClients(i);

	}

	public void provideStatistics( ) {
		for (Client client : clients) {
			try{
			System.out.println(MessageFormat.format(
					"client-{0} consumed {1}",
					client.getName(), client.getOrder().calculateFeedback() ));
			}
			catch(NullPointerException ex){
				
			}
		}

	}

	public boolean invalidNumericCharacter( String text ) {
		int number;
		try {
			number = Integer.parseInt(text);
			if (number > 9 || number <= 0)
				return false;
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

}
