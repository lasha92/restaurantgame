/**
 * @(#) Client.java
 */

package game;

import java.util.ArrayList;
import java.util.Random;
import lombok.Data;

@Data
public class Client extends Person {
	public int volumeConsumed;
	public int calorieConsumed;
	private int phoneNumber;
	private int bill;
	private int taxCode;
	private Orders order;


	public Client( String name ) {
		this.name = name;
		bill = 0;
	}

	public Orders makeOrder( Baverage baverage, Dish dish ) {
		order = new Orders(dish, baverage);
		return order;
	}

	public int calculateSatisfactory( Waiter waiter ) {
		
		int satisfaction = 0; 
		int clientSatisfaction = 0;
		int bonus = 0;
		
		satisfaction = new Random().nextInt(10);
		switch (waiter.getExpLevel()) {
		
		case HIGH: if(satisfaction <= 9) clientSatisfaction++;else clientSatisfaction--; break;
		case MEDIUM:if(satisfaction<=8) clientSatisfaction++;else clientSatisfaction--; break;
		case LOW:if(satisfaction<=6) clientSatisfaction++;else clientSatisfaction--; break;			

		}
		
		if(order.getBaverage().getQuality() == ItemQuality.HIGH)
		{
			bonus+=2;
			for(int i=0;i<((order.getBaverage().getPrice() - order.getDish().getCost())%3);i++)
				bonus++;
		}
		
		satisfaction =  new Random().nextInt(10);
		
		switch(order.getBaverage().getBarman().getExpLevel()){
		
		case LOW: if(bonus+satisfaction <= 4) clientSatisfaction++; else clientSatisfaction--; break;
		case MEDIUM: if(bonus+satisfaction <= 6) clientSatisfaction++; else clientSatisfaction--; break;
		case HIGH: if(bonus+satisfaction <= 8) clientSatisfaction++; else clientSatisfaction--; break;
		
		}
		
		bonus = 0;
		
		if(order.getDish().getQuality() == ItemQuality.HIGH)
		{
			bonus+=2;
			for(int i=0;i<((order.getDish().getPrice() - order.getDish().getCost())%3);i++)
					bonus++;
		}
		
		switch(order.getDish().getChef().getExpLevel()){
		
		case LOW: if(bonus+satisfaction <= 4) clientSatisfaction++; else clientSatisfaction--; break;
		case MEDIUM: if(bonus+satisfaction <= 6) clientSatisfaction++; else clientSatisfaction--; break;
		case HIGH: if(bonus+satisfaction <= 8) clientSatisfaction++; else clientSatisfaction--; break;
		
		}
		
		return clientSatisfaction;

	}

}
