/**
 * @(#) Menu.java
 */

package game;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Menu {
	private List<Dish> dishes;
	private int dishCost;
	private int baverageCost;
	private List<Baverage> baverages;
	private String names;
	private int calories;
	private int volume;
	private int price;
	private int bLowAmount;
	private int bHighAmount = 0;
	private int lowAmount;
	private int highAmount = 0;
	private int dishLOWPrice;
	private int beverageLOWPrice;
	private int dishHIGHPrice;
	private int beverageHIGHPrice;

	public Menu( ) throws Exception {
		dishes = new ArrayList<Dish>();
		baverages = new ArrayList<Baverage>();
	}

	public void createMenu( Chef chef, Barman barman ) throws Exception {
		do {
			System.out.println("You should set menu");
			System.out
					.println("Choose amount of high quality dishes (maximum amount is 5)");
			highAmount = Integer.parseInt(Console.readLine());
			lowAmount = 5 - highAmount;

			System.out
					.println("Choose amount of high quality beverages (maximum amount is 5)");
			bHighAmount = Integer.parseInt(Console.readLine());
			bLowAmount = 5 - bHighAmount;
		} while (highAmount > 5 || bHighAmount > 5);

		System.out.println(MessageFormat.format(
				"Amount of low quality dishes is {0}", lowAmount));
		System.out.println(MessageFormat.format(
				"Amount of low quality beverages is {0}", bLowAmount));

		System.out.println("Enter cost for Low quality dish : ");
		dishLOWPrice = Integer.parseInt(Console.readLine());
		System.out.println("Enter cost for Low quality beverage : ");
		beverageLOWPrice = Integer.parseInt(Console.readLine());

		System.out.println("Enter cost for High quality dish : ");
		dishHIGHPrice = Integer.parseInt(Console.readLine());
		System.out.println("Enter cost for High quality beverage : ");
		beverageHIGHPrice = Integer.parseInt(Console.readLine());
		
		for (int i = 0; i < highAmount; i++) {
			System.out.println("Enter high quality dish name : ");
			names = Console.readLine();

			dishes.add(new Dish(names,chef));
			dishes.get(i).setQuality(ItemQuality.HIGH);
			dishes.get(i).setCost(10);
			dishes.get(i).setPrice(dishHIGHPrice);
		}
		for (int i = 0; i < lowAmount; i++) {
			System.out.println("Enter low quality dish name : ");
			names = Console.readLine();
			dishes.add(new Dish(names,chef));
			dishes.get(i).setCost(3);
			dishes.get(i).setPrice(dishLOWPrice);
		}
		for (int i = 0; i < bHighAmount; i++) {
			System.out.println("Enter high quality beverage name : ");
			names = Console.readLine();
			baverages.add(new Baverage(names,barman));
			baverages.get(i).setQuality(ItemQuality.HIGH);
			baverages.get(i).setCost(3);
			baverages.get(i).setPrice(beverageHIGHPrice);
		}
		for (int i = 0; i < bLowAmount; i++) {
			System.out.println("Enter low quality beverage name : ");
			names = Console.readLine();
			baverages.add(new Baverage(names,barman));
			baverages.get(i).setCost(1);
			baverages.get(i).setPrice(beverageLOWPrice);
		}
		System.out.println("Excellent! Menu created!");

	}
}
