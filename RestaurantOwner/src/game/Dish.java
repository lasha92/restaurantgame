/**
 * @(#) Dish.java
 */

package game;

import lombok.Data;

@Data
public class Dish extends MenuItem {
	private int calories = 10;
	private int price;
	private Chef chef;

	public Dish( String name, Chef chef ) {
		this.chef = chef;
		this.name = name;
		this.calories = 50;
	}

}
